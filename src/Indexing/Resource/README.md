# Generating documentation data
To generate the documentation data, first install [php-doc-parser](https://github.com/martinsik/php-doc-parser). Then
you can run (just use all the defaults for the prompts).

```
vendor/bin/doc-parser --format=json
```
