<?php

namespace A;

class TestClass
{
    const IMPLICITLY_PUBLIC_CONSTANT = 1;
    public const PUBLIC_CONSTANT = 1;
    protected const PROTECTED_CONSTANT = 1;
    private const PRIVATE_CONSTANT = 1;
}
